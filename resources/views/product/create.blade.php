@extends('layout')
@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Create product <small>NOW</small></h3>
        </div>
        <div class="panel-body">
            <form role="form" action="{{ route('product.store') }}" method="POST">
                <div class="form-group">
                    <input type="text" name="title" id="name" class="form-control input-sm" placeholder="Title">
                    @if ($errors->get('title'))
                        <p class="alert-danger">{{ $errors->get('title')[0] }}</p>
                    @endif
                </div>
                <div class="form-group">
                    <input type="number" name="price" id="price" class="form-control input-sm" placeholder="Price">
                    @if ($errors->get('price'))
                        <p class="alert-danger">{{ $errors->get('price')[0] }}</p>
                    @endif
                </div>
                <input type="hidden" value="{{ csrf_token() }}" name="_token">

                <input type="submit" value="Create" class="btn btn-info btn-block">
            </form>
        </div>
    </div>
</div>
@endsection
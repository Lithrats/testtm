<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class ProductFilterController extends Controller
{
    public function index()
    {
        return view('product.index');
    }
    public function search(Request $request)
    {
        $cacheKey = hash('ripemd160', implode(',', array_values($request->input())));

        $products = Cache::remember($cacheKey, 10000, function () use ($request) {
            $query = Product::query();
            $page = $request->get('page');
            $pageSize = $request->get('page_size');

            if ($request->has('title') && $request->get('title') != '') {
                $query = $query->where('title', 'like', '%' . $request->get('title') . '%');
            }
            $checkPriceFrom = $request->has('price_from') && $request->get('price_from') != 'NaN';
            $checkPriceTo = $request->has('price_to') && $request->get('price_from') != 'NaN';

            if ($checkPriceFrom || $checkPriceTo) {
                $query = $query->whereBetween('price', [$request->get('price_from'), $request->get('price_to')]);
            }

            $query->orderBy('id', "DESC");
            $products = $query->paginate($pageSize, ['*'], 'page', $page);
            return $products;
        });

        return response()->json($products);
    }
}

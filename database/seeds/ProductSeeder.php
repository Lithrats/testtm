<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($j = 0; $j < 10000; $j++) {
            $array = [];
            for ($i = 0; $i < 10000; $i++) {
                $array[] = [
                    'title' => $faker->text(rand(10, 20)),
                    'price' => $faker->randomNumber(4),
                ];
            }
            echo $j;
            \App\Product::insert($array);
        }
    }
}
